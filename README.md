# Hedgewars × Super Smash Bros. Ultimate: Hat Collection For Hedgewars
A collection of add-on hats for use with free/open source (FOSS) turned-based stategy game [Hedgewars](https://hedgewars.org) based on various characters appearing in Super Smash Bros. Ultimate (SSBU). Fighters, Kirby's copy ability costumes, assist fighters/trophies, bosses, Poké Ball Pokémon, and Mii hats are all good candidates.

All fighters will likely be covered, but not every assist fighter, boss will be covered, due to some characters having unsuitable designs. Characters that appear only as 2D spirits are ineligible. More hats based on downloadable content (DLC) fighters will be included when their files are ripped, and they are made publicly available.

Google Drive downloads: <https://drive.google.com/drive/folders/0BwRl-upBbiv-M09ZR3ZId3NJX28?usp=sharing>

## Currently Covered Hats
* All of Kirby's copy ability costumes/outfits, excluding Kirby himself. Each hat is a static/unanimated 32x32 PNG image. Updated as of version 10.1.0.

[Preview of all hats](hats_js_anim.html) (the file may need to downloaded first for it to work)
Total number of hats provided so far: 83

## How These Hats Are Made
Unlike most Hedgewars hats, such as those shipping with the game, all of the hats found in this repository are created in Blender 2.80+ from 3D models. 2D images are then rendered directly from these models to create the final hats, after setting up cameras to correctly position each model. As a result, none of the hats match the official Hedgewars style, but it does allow for creating certain kinds of hats more quickly, provided that each setup is done correctly.

## Hat Quirks
* The placement and/or positioning of every hat may actually not be correct; suggestions and fixes are welcome, so that they can look better.

* Some hats can be especially hard to see since they have been shrunken down to such a small resolution (32x32). Zooming in may be necessary to see some of these hats.

* All of the hats are render as they appear in the viewport. As a result, every hat will have some amount of shadows depending on which position they were rendered at.

## Blender Setup
**All of the Blender files found here can be opened only with Blender 2.80 or later. Blender 2.79 and older cannot open them at all; attempting to do so will lead to nothing appearing in the 3D view.**

All source Blender files share the following settings:

* Rendering engine: Eevee (Workbench can be used for models that don't utilize any dual UV map setup)

* World surface: Does not uses nodes, using color (hex) #FFFFFF, or white. Not used anymore since Viewport/OpenGL Render Image is now the preferred method of rendering the hats.

* Studio light: City (brightest studio light image out of all others shipped with Blender), in material preview mode

* Exactly one camera, with image dimensions of 32x32, and using either 'Static_Hat_Template.png' or 'Static_Team_Hat_Template.png' as the background image.

* Film settings: Filter size is 1.00, and uses a transparent background.

### Related Resources
This project follows <https://gitlab.com/Worldblender/smash-ultimate-models-exported> for the status of characters. Head over there to use the characters for things other than kart racing.

## Licensing
**Unless otherwise specified below, everything in './Hats/', and './Source/' is licensed under the CC-BY-NC 4.0 license.** Obviously, however, I do not claim any rights over the characters appearing whom the hats are derived from; this is purely a fan creation. Now comes the specific items that are exempt from noncommercial restrictions:

* Original documentation - CC-BY-SA 4.0

* Everything in './Templates/', './Names/types.ini', and <hats_js_anim.xhtml>: GFDL-1.2

* Everything in './Lights/' and './Names/*.txt': CC0
