## Prerequisites
**Hedgewars 0.9.23 or later is required to use this hats collection. Versions between 0.9.7 and 0.9.22 may work, but adaption is likely required.** If Hedgewars isn't already installed, download it from <https://hedgewars.org/download.html>, and follow the specific instructions for your operating system. If using a Linux/BSD distro, there should be a package for the most recent Hedgewars release; install it from there.

Alternatively, convenience downloads can be found on Google Drive: <https://drive.google.com/drive/folders/0BwRl-upBbiv-M09ZR3ZId3NJX28?usp=sharing>

## How to use/install
All content will be copied into one of the user directories, depending on your operating system:

* On Windows: %USERPROFILE%/Hedgewars/Data/

* On Linux distros/BSDs: ~/.hedgewars/Data/

* On macOS: ~/Library/Application Support/Hedgewars/

Copy the contents of the 'Hats/' directory into the path 'Data/Graphics/Hats'; this part must be completed. Optionally, but recommended, copy the contents of the 'Names/' directory into the path 'Data/Names'; this enables better name selection for some hats. The file 'Names/types.ini' is also optional; it can be copied to enable most of these hats to appear in randomly-generated teams.

Or copy individual hats to the user data directory, if not all of them are desired. Upon starting Hedgewars, and proceeding to edit a team, at least some of the hats should appear in the hedgehog hat. To remove the hat, select “NoHat”.

## Tips
* On a stock Hedgewars installation, none of the installed hats will appear in randomly-generated teams with pre-defined hats by default. However, they can still appear during team editing, and can also be selected manually like with all other hats.

* To have the hats appear in some other modes, such as quick game, also copy the file 'Names/types.ini' to the user data directory path 'Data/Names'. This file will take precedence over the built-in version. It will enable most of these hats to appear in randomly-generated teams.
