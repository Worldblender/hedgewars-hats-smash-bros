# Hats Remaining To Be Worked On
* The full version of every fighter, including a hat/hair version, and a full body/head version for each fighter. Some fighters summon helper characters, who will also be worked on.

* Assist fighters/trophies; not every one of them can be worked on.

* Poké Ball Pokémon; not every one of them can be worked on.

* Bosses; not every one of them can be worked on.

* Mii hats; status remains undetermined, but most likely can be worked on.
